﻿using Goodcredit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Goodcredit.DAO
{
    public class CustomerDAO : DAO
    {
        internal static List<CustomerModel> GetCustomerInfo()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                string command = "SELECT * FROM Customers";
                using (SqlCommand sqlCommand = new SqlCommand(command, sqlConnection))
                {
                    try
                    {
                        sqlConnection.Open();
                        sqlCommand.CommandType = CommandType.Text;
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        List<CustomerModel> customerList = new List<CustomerModel>();
                        CustomerModel customer;
                        while (sqlDataReader.Read())
                        {
                            customer = new CustomerModel();

                            customer.TaskDate = DateTime.Parse(sqlDataReader["TaskDate"].ToString());
                            customer.LateDays = Convert.ToInt32(sqlDataReader["LateDays"].ToString());
                            customer.PhoneNumber = sqlDataReader["PhoneNumber"].ToString();
                            customer.Loan = sqlDataReader["Loan"].ToString();
                            customer.TotalDue = sqlDataReader["TotalDue"].ToString();
                            customer.TaskId = Convert.ToInt32(sqlDataReader["TaskId"].ToString());
                            customer.Id = Convert.ToInt32(sqlDataReader["Id"].ToString());
                            customer.Customer = sqlDataReader["Customer"].ToString();
                            customer.Comment = sqlDataReader["Comment"].ToString();
                            customer.Surplus = Convert.ToDecimal(sqlDataReader["Surplus"].ToString());

                            customerList.Add(customer);
                        }
                        return customerList;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}

