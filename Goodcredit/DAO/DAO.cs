﻿using Goodcredit.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Goodcredit.DAO
{
    public class DAO
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["GoodcreditDB"].ConnectionString;

        internal static List<CustomerModel> Filter(int? filterLateDays, string filterPhoneNumber, string filterLoanNumber, string filterClient, string filterComment)
        {
            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("sp_GetCustomerByFilter", connection))
                {
                    List<CustomerModel> customers = new List<CustomerModel>();
                    try
                    {
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@FilterLateDays", filterLateDays == null ? -1 : filterLateDays);
                        command.Parameters.AddWithValue("@FilterPhoneNumber", filterPhoneNumber == null ? "" : filterPhoneNumber);
                        command.Parameters.AddWithValue("@FilterLoanNumber", filterLoanNumber == null ? "" : filterLoanNumber);
                        command.Parameters.AddWithValue("@FilterClient", filterClient == null ? "" : filterClient);
                        command.Parameters.AddWithValue("@FilterComment", filterComment == null ? "" : filterComment);

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            CustomerModel customer= new CustomerModel();

                            customer.TaskDate = (DateTime)reader["TaskDate"];
                            customer.LateDays = (int)reader["LateDays"];
                            customer.PhoneNumber = reader["PhoneNumber"].ToString();
                            customer.Loan = reader["Loan"].ToString();
                            customer.TotalDue = reader["TotalDue"].ToString();
                            customer.TaskId = (int)reader["TaskId"];
                            customer.Id = (int)reader["Id"];
                            customer.Customer = reader["Customer"].ToString();
                            customer.Comment = reader["Comment"].ToString();
                            customer.Surplus = (decimal)reader["Surplus"];
                            customers.Add(customer);
                        }
                        return customers;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }               
            }            
        }
    }
}