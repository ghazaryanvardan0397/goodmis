﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Goodcredit.DAO
{
    public class ContractAndCloseUserDAO : DAO
    {
        public static int Check_Data(int usid)
        {
            int change_data;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
               
                string command = "SELECT change_date FROM users Where usid = @usid ";
                using (SqlCommand sqlCommand = new SqlCommand(command, connection))
                {
                    try
                    {
                        sqlCommand.Parameters.AddWithValue("@usid", usid);
                        connection.Open();

                        change_data = (int)sqlCommand.ExecuteScalar();
      
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            return change_data;
        }
    }
}