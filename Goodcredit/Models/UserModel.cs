﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Goodcredit.Models
{
    public class UserModel
    {
        [Required(ErrorMessage ="Username is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage ="Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public int? Rights { set; get; }

        public int UsId { get; set; }

        public string Rname { set; get; }

        public int? LoId { get; set; }

        public int? Letters { get; set; }

        public int? Status { get; set; }

        public int Court { get; set; }

        public int? Court_Head { get; set; }

        public string Email { get; set; }

        public int Em_Change { get; set; }

        public int Doc_Upload { get; set; }

        public int Config_Close_Day { get; set; }

        public int Skype_Op  { get; set; }

        public string Skype_User { get; set; }

        public string WinUser { get; set; }

        public int Skn_Change { get; set; }

        public string Skype_Vid_Dir { get; set; }

        public int Black_I_Data { get; set; }

        public int Kys { get; set; }

        public int Custom_Sms { get; set; }

        public int Change_Date { get; set; }

        public int CheckLogin(string username, string password) => DAO.LoginDAO.CheckLogin(username, password);


    }
}