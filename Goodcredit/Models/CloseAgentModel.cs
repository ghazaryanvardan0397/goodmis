﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Goodcredit.Models
{
    public class CloseAgentModel
    {
        [Required(ErrorMessage = "choose partners")]
        public int Partners { get; set; }

        [Required(ErrorMessage = "choose user")]
        public string Name { get; set; }

        [Required(ErrorMessage = "choose branch")]
        public string Branch { get; set; }

       
    }
}