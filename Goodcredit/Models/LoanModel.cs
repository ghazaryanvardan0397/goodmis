﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Goodcredit.Models
{
    public class LoanModel
    {
        public string LoanNumber { get; set; }
        public string Client { get; set; }
        public string PhoneNumber { get; set; }
        public string LoanType { get; set; }
        public string CommentType { get; set; }
        public int? LateDays1 { get; set; }
        public int? LateDays2 { get; set; }
    }
}