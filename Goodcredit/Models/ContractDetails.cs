﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Web;

namespace Goodcredit.Models
{
    public class ContractDetails
    {
        public int TrId { get; set; }
        public bool IsAddition { get; set; }
        public DateTime AprRejDate { get; set; }
        public DateTime SignedDate { get; set; }
        public string DownloadLink { get; set; }
    }
}