﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Goodcredit.Models
{
    public class DropDownListModel<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }
}