﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goodcredit.DAO;

namespace Goodcredit.Models
{
    public class CustomerModel
    {
        public DateTime TaskDate { get; set; }
        public int LateDays { get; set; }
        public string PhoneNumber { get; set; }
        public string Loan { get; set; }
        public string TotalDue { get; set; }
        public int TaskId { get; set; }
        public int Id { get; set; }
        public string Customer { get; set; }
        public string Comment { get; set; }
        public decimal Surplus { get; set; }
        public string PassportOrID { get; set; }
        public string AdditionalPhoneNumber { get; set; }
        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string Address { get; set; }
        public string AdditionalAddress { get; set; }
        public int PreviousLoans { get; set; }
        public int PreviousLates { get; set; }
        public decimal BiggestLoan { get; set; }
        public int LongestLate { get; set; }

        internal static List <CustomerModel> GetCustomer()
        {
            List<CustomerModel> customers = DAO.CustomerDAO.GetCustomerInfo();
            return customers;
        }
        //public static List<CustomerModel> GetCustomerBySearch(string loanNumber, string client, string phoneNumber, /*string loanType,*/ string commentType, string lateDays)
        //{
        //    List<CustomerModel> customers = new List<CustomerModel>();
        //    if(!string.IsNullOrEmpty(loanNumber))
        //    {
        //        customers.AddRange(DAO.DAO.GetCustomerByLoanNumber(loanNumber));
        //    }
        //    if(!string.IsNullOrEmpty(client))
        //    {
        //        customers.AddRange(DAO.DAO.GetCustomerByClient(client));
        //    }
        //    if(!string.IsNullOrEmpty(phoneNumber))
        //    {
        //        //customers.AddRange(DAO.DAO.GetCustomerByPhoneNumber(phoneNumber));
        //    }
        //    //if(!string.IsNullOrEmpty(loanType))
        //    //{
        //    //    customers.AddRange(DAO.DAO.GetCustomerByLoanType(loanType));
        //    //}
        //    if(!string.IsNullOrEmpty(commentType))
        //    {
        //        customers.AddRange(DAO.DAO.GetCustomerByCommentType(commentType));
        //    }
        //    if(!string.IsNullOrEmpty(lateDays))
        //    {
        //        customers.AddRange(DAO.DAO.GetCustomerByLateDays(lateDays));
        //    }

        //    return customers;
        //}
    }
}