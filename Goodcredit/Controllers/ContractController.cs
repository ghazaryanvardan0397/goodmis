﻿using Goodcredit.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Goodcredit.Controllers
{
    public class ContractController : Controller
    {
        [HttpGet]
        public ActionResult GetContract()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetContract(string loanId)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString))
                {
                    await connection.OpenAsync();

                    var command = new SqlCommand
                    {
                        Connection = connection,
                        CommandText = "[circst].[dbo].[ContractGenerationDetails]",
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@loanId", loanId));

                    SqlDataReader reader = await command.ExecuteReaderAsync();

                    List<ContractDetails> contracts = new List<ContractDetails>();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ContractDetails Contract = new ContractDetails
                            {
                                TrId = Convert.ToInt32(reader.GetValue(0)),
                                IsAddition = Convert.ToBoolean(reader.GetValue(1))
                            };

                            if (!DateTime.TryParse(reader.GetValue(2).ToString(), out DateTime result))
                            {
                                result = DateTime.MinValue;
                            }

                            Contract.AprRejDate = result;

                            if (!DateTime.TryParse(reader.GetValue(3).ToString(), out result))
                            {
                                result = DateTime.MinValue;
                            }

                            Contract.SignedDate = result;

                            string blobId = reader.GetValue(4).ToString();
                            Contract.DownloadLink = blobId == "" ? blobId : $"http://10.47.0.135:8088/blobs/{blobId}/raw";

                            contracts.Add(Contract);
                        }
                    }

                    if (contracts.Count != 0)
                    {
                        return View(contracts);
                    }
                    else
                    {
                        return HttpNotFound("No Loans Found");
                    }
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }
    }
}
