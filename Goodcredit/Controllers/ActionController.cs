﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Goodcredit.Controllers
{
    [Authorize]
    public class ActionController : Controller
    {
        // GET: Action
        [HttpGet]
        public ActionResult CustomerInformation()
        {
            return View();
        }
    }
}