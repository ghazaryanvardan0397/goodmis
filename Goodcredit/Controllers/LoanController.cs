﻿using Goodcredit.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;

namespace Goodcredit.Controllers
{
    //[Authorize]
    public class LoanController : Controller
    {
        // GET: Loan
        public ActionResult SelectView()
        {
            return View();
        }
        public ActionResult ManagmentOfLateLoans()
        {
            return View();
        }

        [HttpGet]
        public PartialViewResult _PartialCustomerInfo(int? lateDays, string phoneNumber, string loanNumber, string client, string comment)
        {
            List<CustomerModel> customers = DAO.DAO.Filter(lateDays, phoneNumber, loanNumber, client, comment);
            List<CustomerModel> cm = CustomerModel.GetCustomer();
            if (this.Request.IsAjaxRequest())
            {
                return PartialView(customers);
            }
            return PartialView(cm);
        }
    }
}