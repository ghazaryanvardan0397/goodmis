﻿using Goodcredit.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Goodcredit.Controllers
{
    public class CloseUserController : Controller
    {
      
            string _connectionString = "Data Source=master.am.mssql;Initial Catalog=circst;Integrated Security=True";
            [HttpGet]
            public ActionResult CloseAgent()
            {
                return View();
            }
            [HttpPost]
            public ActionResult CloseAgent(CloseAgentModel model)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(_connectionString))
                        {
                            if (IsClosed(model.Name))
                            {

                                connection.Open();
                                SqlCommand command = new SqlCommand();
                                command.Connection = connection;
                                command.CommandText = "It_CloseAgentUser_Vardan";
                                command.CommandType = System.Data.CommandType.StoredProcedure;
                                command.Parameters.AddRange(new SqlParameter[]{
                                new SqlParameter("@TellerID",model.Name),
                                new SqlParameter("@BranchID",model.Branch)
                        });
                                command.ExecuteNonQuery();
                            }
                            else
                            {
                                ViewBag.Error = "User already is closed";
                                return View(model);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                        return View(model);
                    }
                    ViewBag.Success = "User is closed";
                }

                return View(model);
            }

            private bool IsClosed(string name)
            {
                int status = 1;
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandText = "select [status] from[dbo].[Ofline_users] where teller_id = @tellerId";
                    SqlParameter tellerIid = new SqlParameter("@tellerId", name);
                    command.Parameters.Add(tellerIid);
                    status = (int)command.ExecuteScalar();
                }
                return status == 0 ? true : false;
            }

            public JsonResult GetPartners()
            {
                List<DropDownListModel<int>> list = new List<DropDownListModel<int>>();
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "select [id],[name] from [dbo].[Ofline_partners]";
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DropDownListModel<int> listModel = new DropDownListModel<int>();
                        listModel.Id = (int)reader.GetValue(0);
                        listModel.Name = (string)reader.GetValue(1);
                        list.Add(listModel);
                    }
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }

            public JsonResult GetBranch(int id)
            {
                List<DropDownListModel<string>> list = new List<DropDownListModel<string>>();
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "select [id],[name] from [dbo].[Ofline_branch] where [pid]=@pid";
                    SqlParameter param = new SqlParameter("@pid", id);
                    command.Parameters.Add(param);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DropDownListModel<string> listModel = new DropDownListModel<string>();
                        listModel.Id = (string)reader.GetValue(0);
                        listModel.Name = (string)reader.GetValue(1);
                        list.Add(listModel);
                    }
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }

            public JsonResult GetTellers(string branch)
            {
                List<DropDownListModel<int>> list = new List<DropDownListModel<int>>();
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "select [id],[name],[login],[status] from [dbo].[Ofline_p_tellers]" +
                                          " inner join[dbo].[Ofline_users] on [dbo].[Ofline_users].teller_id=[dbo].[Ofline_p_tellers].id" +
                                          " where branch = @br";
                    SqlParameter param = new SqlParameter("@br", branch);
                    command.Parameters.Add(param);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DropDownListModel<int> listModel = new DropDownListModel<int>();
                        listModel.Id = (int)reader.GetValue(0);
                        listModel.Name = (string)reader.GetValue(1) + " ( " + (string)reader.GetValue(2) + " " + (((int)reader.GetValue(3) == 0) ? "'open'" : "'closed'") + " )";
                        list.Add(listModel);
                    }
                }
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        
    }
}