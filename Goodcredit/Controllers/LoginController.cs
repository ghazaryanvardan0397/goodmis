﻿using Goodcredit.DAO;
using Goodcredit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace Goodcredit.Controllers
{
    
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult LoginPage()
        {
            return View();
        }

        [HttpPost]
        public ViewResult LoginPage(string username, string password)
        {
            UserModel um = new UserModel();


            int result = um.CheckLogin(username, password);
            if (result!=0)
            {
                int checkDate = ContractAndCloseUserDAO.Check_Data(result);
                ViewBag.ChachDate = checkDate;
                return View("../Loan/SelectView");
                
            }
            ViewBag.Error = "Login Failed";

            return View();


        }

       
       
    }
}